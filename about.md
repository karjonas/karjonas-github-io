---
layout: page 
title: About 
---
Welcome to the personal site of [Jonas Karlsson](/about/).

I am a professional software developer living in Geneva, Switzerland. I am currently working on visualization at the [Blue Brain Project](https://bluebrain.epfl.ch/). Feel free to check out my [projects](/projects/) and my [articles](/).

